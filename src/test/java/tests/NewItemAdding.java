package tests;

import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import preconditions.*;
import utils.BrowserSettings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@SuppressWarnings("FieldCanBeLocal")
public class NewItemAdding extends BrowserSettings {
    //Data to enter
    private static final String email = "webinar.test@gmail.com";
    private static final String password = "Xcg7299bnSmMuRLp9ITw";

    private final String windowClosureFail = "The popup window hasn't been closed";

    private GoodsDetailsPage itemDetailsPage;
    private MainPage mainPage;
    private NewItemPage newItemPage;
    private ItemGenerator itemGenerator;
    private AdminDashboardPage dashboardPage;
    private ItemsPage itemsPage;
    private SoftAssert softAssert;

    @BeforeClass
    public void createInstances() {
        itemDetailsPage = new GoodsDetailsPage(driver);
        mainPage = new MainPage(driver);
        newItemPage = new NewItemPage(driver);
        itemGenerator = new ItemGenerator();
        dashboardPage = new AdminDashboardPage(driver);
        itemsPage = new ItemsPage(driver);
        softAssert = new SoftAssert();
    }

    @BeforeClass
    public void getItemsData() {
        Reporter.log("Getting the random item (name, quantity, price)");
        itemGenerator.generateItem();
        itemGenerator.generatePrice();
        itemGenerator.generateQuantity();
    }

    @DataProvider
    public Object[][] getRegistrationData() {
        return new String[][] {
                { email, password }
        };
    }

    @Test(dataProvider = "getRegistrationData")
    public void dashboardPageShouldGetOpenedAfterLogin(String email, String password) {
        AdminLoginPage authorization = new AdminLoginPage(driver);
        AdminDashboardPage adminDashboardPage = new AdminDashboardPage(driver);
        authorization.login(email, password);
        assertTrue(adminDashboardPage.isDashboardPageOpened());
    }

    @Test(dependsOnMethods = "dashboardPageShouldGetOpenedAfterLogin")
    public void shouldAddTheItem() {
        dashboardPage.proceedToCatalogueSubmenu();
        newItemPage = itemsPage.clickAddNewItemButton();
        newItemPage.addNewItem();

        softAssert.assertTrue(newItemPage.isPopUpWindowClosed(), windowClosureFail);
        newItemPage.clickSaveButton();
        softAssert.assertTrue(newItemPage.isPopUpWindowClosed(), windowClosureFail);
    }

    @Test(dependsOnMethods = "shouldAddTheItem")
    public void shouldNavigateToMainPage() {
        assertTrue(newItemPage.isMainPageOpened(), "The main page hasn't been opened.");
    }

    @Test(dependsOnMethods = "shouldNavigateToMainPage")
    public void shouldCheckIfTheGoodsAreAdded() throws InterruptedException {
        mainPage = new MainPage(driver);
        mainPage.clickAllGoodsLink();
        assertTrue(mainPage.isItemDisplayed(), "The item hasn't been found");
    }

    @Test(dependsOnMethods = "shouldCheckIfTheGoodsAreAdded")
    public void ItemShouldHaveTheProperName() {
        assertEquals(itemDetailsPage.getItemNameDisplayed(), ItemGenerator.getItem().toUpperCase(), "The name of the item is not displayed properly");
    }

    @Test(dependsOnMethods = "shouldCheckIfTheGoodsAreAdded")
    public void ItemShouldHaveTheProperPrice() {
        assertEquals(itemDetailsPage.getPriceDisplayed(), ItemGenerator.getPrice() + " ₴", "The price of the item is not displayed properly");
    }

    @Test(dependsOnMethods = "shouldCheckIfTheGoodsAreAdded")
    public void ItemShouldHaveTheProperQuantity() {
        assertEquals(itemDetailsPage.getQuantityDisplayed(), ItemGenerator.getQuantity(), "The quantity of the items is not displayed properly");
    }
}