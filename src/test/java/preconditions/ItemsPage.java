package preconditions;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

@SuppressWarnings({"ALL", "CanBeFinal"})
public class ItemsPage {
    private WebDriver driver;

    public ItemsPage(WebDriver driver) {
        this.driver = driver;
    }

    //Locators
    private String newGoodsButtonLocator = "page-header-desc-configuration-add";
    @Getter
    private String itemName = "form[step1][name][1]";

    public NewItemPage clickAddNewItemButton() {
        Reporter.log("Clicking the \"New Item\" button");
        driver.findElement(By.id(newGoodsButtonLocator)).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name(itemName)));
        return new NewItemPage(driver);
    }
}
