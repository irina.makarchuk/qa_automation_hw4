package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

@SuppressWarnings({"ALL", "CanBeFinal"})
public class AdminDashboardPage {
    private WebDriver driver;
    private WebDriverWait wait;

    private By catalogue = By.id("subtab-AdminCatalog");
    private By catalogueSubmenu = By.xpath("//li[@id='subtab-AdminCatalog']/ul");
    private String goodsSubmenuLocator = "//li[@id='subtab-AdminProducts']/a";

    public AdminDashboardPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
    }

    private void makeElementActive(By by) {
        Actions builder = new Actions(driver);
        WebElement element = driver.findElement(by);
        builder.moveToElement(element).build().perform();
    }
    private void goToCatalogueSubmenu() {
        Reporter.log("Opening the catalogue submenu");
        makeElementActive(catalogue);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(catalogueSubmenu)));
        makeElementActive(catalogueSubmenu);
    }

    private void clickGoodsSubmenu() {
        Reporter.log("Clicking the \"Goods\" submenu");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(goodsSubmenuLocator)));
        driver.findElement(By.xpath(goodsSubmenuLocator)).click();
        wait.until(ExpectedConditions.titleContains("товары"));
    }

    public boolean isDashboardPageOpened() {
        return driver.getTitle().contains("Dashboard");
    }

    public void proceedToCatalogueSubmenu() {
        goToCatalogueSubmenu();
        clickGoodsSubmenu();
        new ItemsPage(driver);
    }
}
