package preconditions;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

@SuppressWarnings("FieldCanBeLocal")
@Slf4j
@Getter
public class NewItemPage {
    private WebDriver driver;
    private WebElement element;

    //Locators
    private String quantityFieldLocator = "form_step1_qty_0_shortcut";
    private String itemPriceLocator = "form_step1_price_shortcut";
    private String switcherLocator = "switch-input";
    private String popupWindowLocator = "//div[contains(text(), 'Настройки обновлены')]";
    private String popupWindowCloseLocator = "//div[@id='growls']//div[@class='growl-close']";
    private String saveButtonLocator = "//button[@class='btn btn-primary js-btn-save']";

    public NewItemPage(WebDriver driver) {
        this.driver = driver;
    }

    private void insertItemName() {
        Reporter.log("Inserting the item name");
        ItemsPage itemsPage = new ItemsPage(driver);
        driver.findElement(By.name(itemsPage.getItemName())).sendKeys(ItemGenerator.getItem());
    }

    private void insertItemsQuantity() {
        Reporter.log("Inserting the quantity");
        element = driver.findElement(By.id(quantityFieldLocator));
        element.clear();
        element.sendKeys(String.valueOf(ItemGenerator.getQuantity()));
    }

    private void insertItemsPrice() {
        Reporter.log("Inserting the price");
        element = driver.findElement(By.id(itemPriceLocator));
        element.clear();
        element.sendKeys(ItemGenerator.getPrice());
    }

    private void turnOnTheSwitcher() {
        Reporter.log("Turning the switcher on");
        driver.findElement(By.className(switcherLocator)).click();
    }

    public boolean isPopUpWindowClosed() {
        Reporter.log("Closing the pop up");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(popupWindowLocator)));
        element = driver.findElement(By.xpath(popupWindowCloseLocator));
        element.click();
        return wait.until(ExpectedConditions.stalenessOf(element));
    }

    public void clickSaveButton() {
        Reporter.log("Clicking the \"Save\" button");
        driver.findElement(By.xpath(saveButtonLocator)).click();
    }

    public void addNewItem() {
        insertItemName();
        insertItemsQuantity();
        insertItemsPrice();
        turnOnTheSwitcher();
    }

    public boolean isMainPageOpened() {
        Reporter.log("Opening the main page");
        driver.get("http://prestashop-automation.qatestlab.com.ua/ru/");
        return driver.getTitle().equals("prestashop-automation");
    }
}